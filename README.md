# Price index dataviz


## Context

Our price index uses the trades and orderbooks data from a variety of exchanges and each second, we compute a weighed average of the prices found on each exchange. To do so, we elect a 'market mover': the exchange driving the global bitcoin price up or down. In the weights distribution, the market mover get a 50% weight and the remaining 50% are shared among the other exchanges. (More info on the price index computation can be found <a href='https://kaiko.com/bitcoin-price'>here</a>)


## Goal

We'd like to see for a complete hour which exchanges were the market movers, the global weight distribution and how the changes in the market mover affected the price index (i.e. when a new market mover is elected, the weights change brutally which may cause in steep change in the price).


## To-dos

To visualize this, we suggest the following:
- 1- draw a line chart displaying the price over time
- 2- draw a stacked area chart showing the exchanges weights distribution
- 3- find a user friendly way of displaying which exchange is the market mover using hover behaviors
- 4- highlight on the price chart the moments when the market mover changes (basically when a new exchange get a 0.5 weight).


## Tools

The data.csv file in this repo contains the complete list of prices and weights computed for a bit less than an hour.
The weights are in the form of a JSON object with the following format:

```
{
  'be_btceur': 3.7465812515703795E-5,
  'be_btcusd': 0.003561951791749553,
  'bf_btcusd': 0.025787100370696498,
  'bs_btcusd': 0.012465317821356289,
  'cb_btceur': 0.002716677368728479,
  'cb_btcusd': 0.002222375845134446,
  'gi_btcusd': 0.13546911850824214,
  'hb_btccny': 0.5, // Huobi/btccny is the current market leader
  'hb_btcusd': 0.006069869855898996,
  'ib_btceur': 0.20279359277655076,
  'ib_btcusd': 0.0048045475033218505,
  'kk_btceur': 0.007785674030846191,
  'kk_btcusd': 3.7494254397478474E-7,
  'oc_btccny': 0.0486822771774177,
  'oc_btcusd': 0.04760365619499733
}
```

Each key represents an exchange's slug following by its trading currency pair. For example, ```cb_btcusd``` represents the weight of coinbase for its trades in USD.

To start working, just run ```npm i``` followed by ```npm start``` and start editing `main.js`. Your results will be visible on port `8000`: `http://localhost:8000`
