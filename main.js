/* global d3 */
(function() {
  d3.select('body')
    .append('text')
    .text('Let\'s get this started');

  d3.csv('./data.csv', jsonify, function(data) {
    data.forEach(function(d) {
      console.log(d)
    })
  })

  function jsonify (d) {
    d.weights = JSON.parse(d.weights.replace(/"/g, '').replace(/'/g, '"'))
    return d
  }
})()